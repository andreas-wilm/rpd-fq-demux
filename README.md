# A Custom FastQ demultiplexer

Developed for analyzing shRNA libraries containing a special construct.

See [demux.html](demux.html) for auto-generated docs.

The repo contains a slightly modified copy of Jonathan Badger's [nimbioseq](https://github.com/jhbadger/nimbioseq).

Have mercy: this is my first Nim program
