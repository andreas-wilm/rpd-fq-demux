#!/bin/bash


error() {
    echo "ERROR: $@" 1>&2
}

demux=$(ls ../demux) || exit 1
pref=$(mktemp); rm $pref; pref="${pref}-"
echo "Tests are only completed, if final exit code is 0"



fqin=test-acgt.fastq.gz

$demux -i $fqin --bc_start_idx 2 --bc_end_idx 2 --clipAfter -o $pref || exit 1
# Should create one file with an A
bc="C"
base="A"
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 1 ]; then
    error "Expected one files, but got $nout"
    exit 1
fi
if [ $out != "${pref}${bc}.fastq.gz" ]; then
    error "Unexpected output file name $out"
    exit 1
fi
bases=$(gzip -dc $out | awk '{if (NR%4==2) {print $0}}')
if [ $bases != "$base" ]; then
    error "At least one unexpected base in $out: $bases"
    exit 1
fi
rm ${pref}*


$demux -i $fqin --bc_start_idx 3 --bc_end_idx 3 --clipBefore -o $pref || exit 1
# Should create one file with an T
bc="G"
base="T"
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 1 ]; then
    error "Expected one file, but got $nout"
    exit 1
fi
if [ $out != "${pref}${bc}.fastq.gz" ]; then
    error "Unexpected output file name $out"
    exit 1
fi
bases=$(gzip -dc $out | awk '{if (NR%4==2) {print $0}}')
if [ $bases != "$base" ]; then
    error "Unexpected base in $out: $bases"
    exit 1
fi
rm ${pref}*


fqin=test-nactg.fastq.gz

$demux -i $fqin --bc_start_idx 1 --bc_end_idx 2 -o $pref || exit 1
# should not create any files (N's in barcode disallowed)
out=$(ls ${pref}* 2>/dev/null)
nout=$(echo $out | wc -w)
if [ $nout -ne 0 ]; then
    error "Expected zero files, but got $nout"
    exit 1
fi

$demux -i $fqin --bc_start_idx 1 --bc_end_idx 2 --inclBarcodesWithNs -o $pref || exit 1
# should not create any files (N's in barcode disallowed)
out=$(ls ${pref}* 2>/dev/null)
nout=$(echo $out | wc -w)
if [ $nout -ne 1 ]; then
    error "Expected one file, but got $nout"
    exit 1
fi
rm ${pref}*



fqin=test-4seqs-len8-rotated.fastq.gz

$demux -i $fqin --bc_start_idx 1 --bc_end_idx 2 -o $pref || exit 1
# Should create four files with 8 bases each (as input fq, since no clipping enabled)
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 4 ]; then
    error "Expected four files, but got $nout"
    exit 1
fi
nbases=$(gzip -dc $out | awk '{if (NR%4==2) {print length($0)}}' | sort -u)
if [ $nbases -ne 8 ]; then
    error "Expected eight bases, but got $nbases"
    exit 1
fi
rm ${pref}*


$demux -i $fqin --bc_start_idx 1 --bc_end_idx 2 --clipAfter -o $pref  || exit 1
# should not create any files (barcode and sq are clipped and nothing is left
out=$(ls ${pref}* 2>/dev/null)
nout=$(echo $out | wc -w)
if [ $nout -ne 0 ]; then
    error "Expected zero files, but got $nout"
    exit 1
fi


$demux -i $fqin --bc_start_idx 7 --bc_end_idx 8 -o $pref || exit 1
# Should create four files with 8 bases each (as input fq, since no clipping enabled)
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 4 ]; then
    error "Expected four files, but got $nout"
    exit 1
fi
nbases=$(gzip -dc $out | awk '{if (NR%4==2) {print length($0)}}' | sort -u)
if [ $nbases -ne 8 ]; then
    error "Expected eight bases, but got $nbases"
    exit 1
fi
rm ${pref}*


$demux -i $fqin --bc_start_idx 7 --bc_end_idx 8 --clipBefore -o $pref || exit 1
# should not create any files (barcode and sq are clipped and nothing is left
out=$(ls ${pref}* 2>/dev/null)
nout=$(echo $out | wc -w)
if [ $nout -ne 0 ]; then
    error "Expected zero files, but got $nout"
    exit 1
fi


$demux -i $fqin --bc_start_idx 3 --bc_end_idx 6 -o $pref || exit 1
# Should create four files with 8 bases each (as input fq, since no clipping enabled)
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 4 ]; then
    error "Expected four files, but got $nout"
    exit 1
fi
nbases=$(gzip -dc $out | awk '{if (NR%4==2) {print length($0)}}' | sort -u)
if [ $nbases -ne 8 ]; then
    error "Expected eight bases, but got $nbases"
    exit 1
fi
rm ${pref}*


$demux -i $fqin --bc_start_idx 3 --bc_end_idx 6 --clipBefore -o $pref || exit 1
# Should create four files with 2 bases each (as input fq, since no clipping enabled)
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 4 ]; then
    error "Expected four files, but got $nout"
    exit 1
fi
nbases=$(gzip -dc $out | awk '{if (NR%4==2) {print length($0)}}' | sort -u)
if [ $nbases -ne 2 ]; then
    error "Expected eight bases, but got $nbases"
    exit 1
fi
rm ${pref}*


$demux -i $fqin --bc_start_idx 3 --bc_end_idx 6 --clipAfter -o $pref || exit 1
# Should create four files with 2 bases each (as input fq, since no clipping enabled)
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 4 ]; then
    error "Expected four files, but got $nout"
    exit 1
fi
nbases=$(gzip -dc $out | awk '{if (NR%4==2) {print length($0)}}' | sort -u)
if [ $nbases -ne 2 ]; then
    error "Expected eight bases, but got $nbases"
    exit 1
fi
rm ${pref}*



$demux -i $fqin --bc_start_idx 1 --bc_end_idx 2 --validBarcodesFile bc.txt -o $pref || exit 1
# Should create two files 
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 2 ]; then
    error "Expected two files, but got $nout"
    exit 1
fi
bc=$(gzip -dc $out | awk '{if (NR%4==2) {print substr($0, 1, 2)}}')
if [ "$bc" != "$(cat bc.txt)" ]; then
    error "Output barcodes don't match contents of bc.txt"
    exit 1
fi
rm ${pref}*



fqin=test-bc-1-2-ident.fastq.gz

$demux -i test-bc-1-2-ident.fastq.gz --bc_start_idx 1 --bc_end_idx 2 -o $pref || exit 1
# Should create one file, since all seqs have the same bc at [1,2]
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -ne 1 ]; then
    error "Expected four files, but got $nout"
    exit 1
fi
rm ${pref}*


$demux -i test-bc-1-2-ident.fastq.gz --bc_start_idx 1 --bc_end_idx 3 -o $pref || exit 1
# Should create more than one file, since (identical) runs from [1,2]
out=$(ls ${pref}*)
nout=$(echo $out | wc -w)
if [ $nout -lt 2 ]; then
    error "Expected at least 2 files, but got $nout"
    exit 1
fi
rm ${pref}*


echo "OK: all tests passed"

