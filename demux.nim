## Custom FastQ demultiplexer developed for analyzing shRNA libraries
## containing a special construct
##
## - Author: Andreas Wilm <wilma@gis.a-star.edu.sg>
## - License: The MIT License


import cligen
import logging
import os
import tables
import strutils
import zip/gzipfiles
#import nimprof

# FIXME local copy
import biosequence

# From the docs "Warning: The global list of handlers is a thread var,
# this means that the handlers must be re-added in each thread."
var L = newConsoleLogger()
addHandler(L)

var
    numReadsIn = 0
    numReadsOut = 0
    numReadsBuffered = 0


proc writeFastQs(filename: string, sqs: seq[Record]) =
  ## Appends sequences of FastQ Records to gzipped filename
  let stream = newGZFileStream(filename, fmAppend)
  if stream == nil:
    fatal("Unable to open file ", filename)
    quit(QuitFailure)
  #debug("Appending ", len(sqs), " sequences to ", filename)
  for sq in sqs:
    write(stream, toFastq(sq) & "\n")
  close(stream)


proc fastqFilename(outPrefix: string, barcode = "*"): string =
  ## returns FastQ output filename for with `outPrefix` and `barcode`.
  ## default barcode results in return of a glob.
  outPrefix & barcode & ".fastq.gz"


proc outprefMatchesExist(outPrefix: string): bool =
  ## returns true if any files exist that start with `outPrefix`
  for fn in walkFiles(fastqFilename(outPrefix)):
    return true
  return false


proc processSeqTable(seqTable: var Table, outPrefix: string) =
  ## Processes `seqTable` (which has sequence of FastQ records as
  ## value and barcodes as key) by writing the entries to their
  ## corresponding files. Clears table afterwards and updates
  ## `numReadsBuffered` and `numReadsOut`
  for barcode, sqs in seqTable.pairs:
    let filename = fastqFilename(outPrefix, barcode)
    if len(sqs)>0:
      writeFastQs(filename, sqs)
  clear(seqTable)
  numReadsOut += numReadsBuffered
  numReadsBuffered = 0


proc clipSq(sq: Record, bcStartIdx: int, bcEndIdx: int,
            clipBefore = false, clipAfter = false): Record =
  ## clip `sq` such that the barcode (with predetermined positions
  ## `bcStartIdx` and `bcEndIdx` is removed. optionally clip anything
  ## before or after barcode as well.
  var
    sqClipped: Record = sq
    sqStart: int
    sqEnd: int

  (sqStart, sqEnd) = (0, len(sq.sequence)-1)
  if clipBefore:
    sqStart = bcEndIdx + 1
  if clipAfter:
    sqEnd = bcStartIdx-1# -1 to exclude

  # skip if nothing would be left
  if sqStart == length(sq) or sqEnd < sqStart:
    sqClipped = Record()# reset
    return sqClipped

  # FIXME would be nicer to extend nimbioseq's Record to support slicing
  sqClipped.sequence = sqClipped.sequence[sqStart..sqEnd]
  sqClipped.quality = sqClipped.quality[sqStart..sqEnd]

  return sqClipped


proc demux*(fastQ: string, bcStartIdx: int, bcEndIdx: int, outPrefix: string,
            validBarcodes: seq[string] = @[], clipBefore = false, clipAfter = false,
            inclBarcodesWithNs = false, readBufferSize = 1000000, beepEveryNReads = 100000) =
  ## Parses file `fastQ` and appends (!) contained sequences according
  ## to their barcode to files starting with `outPrefix`. Clipping of
  ## sequences before/after the barcode (including the barcode) is
  ## enabled with `clipBefore`/`clipAfter` resp.
  var
    seqTable = initTable[string, seq[Record]]()

  doAssert bcStartIdx >= 0
  doAssert bcEndIdx >= bcStartIdx
  doAssert existsFile(fastQ)

  # main loop
  #
  for sq in readSeqs(fastQ):
    var sqClipped: Record
    inc(numReadsIn)

    if (beepEveryNReads > 0) and (numReadsIn mod beepEveryNReads == 0):
      info("Processing read number ", numReadsIn)

    if len(sq.sequence) <= bcEndIdx:
      continue
    let barcode = toUpperAscii(sq.sequence[bcStartIdx..bcEndIdx])

    if len(validBarcodes) > 0 and not validBarcodes.contains(barcode):
      continue

    if inclBarcodesWithNs == false and barcode.contains('N'):
      continue

    # clipping
    sqClipped = clipSq(sq, bcStartIdx, bcEndIdx, clipBefore, clipAfter)
    if len(sqClipped.sequence) == 0:
      continue

    if not seqTable.hasKey(barcode):
      var ss = newSeq[Record]()
      seqTable[barcode] = ss

    assert length(sqClipped) > 0
    seqTable[barcode].add(sqClipped)
    inc(numReadsBuffered)

    if numReadsBuffered == readBufferSize:
      info("Writing to files...")
      processSeqTable(seqTable, outPrefix)

  # process remaining ones
  processSeqTable(seqTable, outPrefix)


proc main(fastQ: string, bcStartIdx: int, bcEndIdx: int, outPrefix: string,
          validBarcodesFile: string = nil, clipBefore = false, clipAfter = false,
          inclBarcodesWithNs = false, readBufferSize = 1000000, beepEveryNReads = 100000) =
  ## Wrapper for demux(), with the only difference that barcode slice
  ## coordinates (`bcStartIdx1` and `bcEndIdx1`) here are expected to
  ## come from user i.e. one based, closed interval!
  var validBarcodes: seq[string] = @[]

  # sanity checks
  #
  if bcStartIdx < 1 or bcEndIdx < bcStartIdx:
    fatal("Invalid choice of barcode index positions")
    quit(QuitFailure)
  if not existsFile(fastQ):
    fatal("FastQ file '", fastQ, "' does not exist")
    quit(QuitFailure)
  # we append by default, so no output file is allowed to pre-exist
  if outprefMatchesExist(outPrefix):
    error("Found files matching output prefix. Please delete first!")
    quit(QuitFailure)

  if not validBarcodesFile.isNilOrEmpty():
    for line in lines validBarcodesFile:
      validBarcodes.add(line.strip())

  demux(fastQ, bcStartIdx - 1, bcEndIdx - 1, outPrefix, validBarcodes,
        clipBefore, clipAfter, inclBarcodesWithNs,
        readBufferSize, beepEveryNReads)

  info("Number of input reads = ", numReadsIn)
  info("Number of output reads = ", numReadsOut)


when isMainModule:
  import cligen
  dispatch(main, help = {
    "bcEndIdx":  "End-index for barcode (>=bcStartIdx)",
    "bcStartIdx" : "Start-index of barcode (>=1)",
    "beepEveryNReads": "Be verbose and report status every x reads (0 to disable)",
    "clipAfter": "Clip barcode and sequence after (without any clipping, sequence is written as-is)",
    "clipBefore": "Clip barcode and sequence before (without any clipping, sequence is written as-is)",
    "fastQ": "FastQ input file (single-end)",
    "inclBarcodesWithNs": "Allow Ns in barcode (skipped by default)",
    "outPrefix": "Prefix used for all created FastQ files",
    "readBufferSize": "Keep this many reads in memory before writing to files",
    "validBarcodesFile": "File containing valid barcodes, one per line (ignores others)",
    },
    short = {
      "bcEndIdx": 'e',
      "bcStartIdx": 's',
      "clipAfter": 'a',
      "clipBefore": 'b',
      "fastQ": 'i',
      "inclBarcodesWithNs": 'n'
      }
  )
